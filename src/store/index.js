import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    posts: [
      {txt: 'Прошу извинить за то, что назвал вас шайтаном', cheked: false, id: 40934564356},
      {txt: 'Кликни два раза, чтобы изменить текст', cheked: false, id: 40934564357},
    ],
  },
  getters: {
    posts(state) {
      return state.posts;
    },
    count(state) {
      return state.posts.length;
    },
    succes(state) {
      return state.posts.filter(item => item.cheked == true).length;
    },
  },
  mutations: {
    addItem(state, item) {
      state.posts = [...state.posts, item];
    },
    deleteItem(state, id) {
      let idx = state.posts.findIndex(item => item.id == id);
      let after = state.posts.slice(0, idx);
      let before = state.posts.slice(idx + 1);
      state.posts = [...after,...before];
    },
    chekedItem(state, id) {
      let idx = state.posts.findIndex(item => item.id == id);
      let after = state.posts.slice(0, idx);
      let before = state.posts.slice(idx + 1);
      let newItem = {...state.posts[idx], cheked: !state.posts[idx].cheked};
      state.posts = [...after, newItem, ...before];
    },
    updateItem(state, newData) {
      state.posts = Object.assign(state.posts, newData);
      console.log(state.posts)
    },
  },
  actions: {
    
  },
})
